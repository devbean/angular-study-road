# Angular Study Road

#### 介绍
Angular 学习之路

#### 目录说明
* demo - Angular CLI 生成的默认项目
* demo-app - Angular demo 项目
* todomvc-app-template - todomvc 模板文件
* todomvc-app - todomvc Angular 实现
