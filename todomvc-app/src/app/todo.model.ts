export interface Todo {

  id: number;

  content: string;

  completed: boolean;

  editing: boolean;

}
