import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { combineLatest, Subject, Subscription } from 'rxjs';
import { TodoService } from '../todo.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit, OnDestroy {

  @Input() uncompletedCount = 0;
  @Input() completedCount = 0;

  private readonly unsubscribe$: Subject<void> = new Subject<void>();

  constructor(
    private readonly todoService: TodoService
  ) { }

  ngOnInit(): void {
    // this.todoService.todos$
    //   .pipe(
    //     takeUntil(this.unsubscribe$)
    //   )
    //   .subscribe(todos => {
    //     this.leftCount = todos.filter(it => !it.completed).length;
    //   });
    // this.todoService.count$
    //   .pipe(
    //     takeUntil(this.unsubscribe$)
    //   )
    //   .subscribe(count => {
    //     this.todoCount = count;
    //   });

    // combineLatest([ this.todoService.todos$, this.todoService.count$ ])
    //   .pipe(
    //     takeUntil(this.unsubscribe$)
    //   )
    //   .subscribe(data => {
    //     const todos = data[0];
    //   });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onClearCompletedClicked(): void {
    this.todoService.updateAllComplete(false);
  }

}
