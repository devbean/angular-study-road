import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Todo } from '../todo.model';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit, OnDestroy {

  @Input() todos: Todo[] = [];

  private subscription = Subscription.EMPTY;

  constructor(
    private readonly todoService: TodoService
  ) { }

  ngOnInit(): void {
    // this.subscription = this.todoService.todos$
    //   .subscribe(todos => {
    //     this.todos = todos;
    //   });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  trackById(index, item): number {
    return item.id;
  }

  onCompleteAllClicked(): void {
    this.todoService.updateAllComplete();
  }

}
