import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo-creator',
  templateUrl: './todo-creator.component.html',
  styleUrls: ['./todo-creator.component.css']
})
export class TodoCreatorComponent implements OnInit {

  content: string;

  constructor(
    private readonly todoService: TodoService
  ) { }

  ngOnInit(): void {
  }

  onEnterKey(): void {
    this.todoService.add(this.content);
    this.content = '';
  }

}
