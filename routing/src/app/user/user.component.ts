import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  id = -1;

  constructor(
    private readonly route: ActivatedRoute
  ) {
    this.route.paramMap
      .subscribe(params => {
        this.id = +(params.get('id') ?? '-1');
      });
  }

  ngOnInit(): void {
  }

}
