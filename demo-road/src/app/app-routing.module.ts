import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'ngfor', loadChildren: () => import('./ngfor/ngfor.module').then(m => m.NgforModule) },
      { path: 'ngswitch', loadChildren: () => import('./ngswitch/ngswitch.module').then(m => m.NgswitchModule) },
      { path: 'myclass', loadChildren: () => import('./my-class/my-class.module').then(m => m.MyClassModule) },
      { path: 'myif', loadChildren: () => import('./my-if/my-if.module').then(m => m.MyIfModule) },
      { path: 'tooltip', loadChildren: () => import('./tooltip/tooltip.module').then(m => m.TooltipModule) }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
