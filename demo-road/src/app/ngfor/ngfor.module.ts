import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SyntaxComponent } from './syntax/syntax.component';
import { NgForRoutingModule } from './ngfor-routing.module';
import { TableComponent } from './table/table.component';
import { LocalVarsComponent } from './local-vars/local-vars.component';



@NgModule({
  declarations: [
    SyntaxComponent,
    TableComponent,
    LocalVarsComponent
  ],
  imports: [
    CommonModule,
    NgForRoutingModule
  ]
})
export class NgforModule { }
