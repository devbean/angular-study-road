import { Component, OnInit } from '@angular/core';


interface Employee {
  name: string;
  email: string;
  skills: {
    skill: string;
    exp: string;
  }[];
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  employees: Employee[] = [
    {
      name: 'Rahul', email: 'rahul@gmail.com',
      skills: [{skill: 'Angular', exp: '2'}, {skill: 'Javascript', exp: '7'}, {skill: 'TypeScript', exp: '3'}]
    },
    {
      name: 'Sachin', email: 'sachin@gmail.com',
      skills: [{skill: 'Angular', exp: '1'}, {skill: 'Android', exp: '3'}, {skill: 'React', exp: '2'}]
    },
    {
      name: 'Laxmna', email: 'laxman@gmail.com',
      skills: [{skill: 'HTML', exp: '2'}, {skill: 'CSS', exp: '2'}, {skill: 'Javascript', exp: '1'}]
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
