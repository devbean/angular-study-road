import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalVarsComponent } from './local-vars.component';

describe('LocalVarsComponent', () => {
  let component: LocalVarsComponent;
  let fixture: ComponentFixture<LocalVarsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocalVarsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalVarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
