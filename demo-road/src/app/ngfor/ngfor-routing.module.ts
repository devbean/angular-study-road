import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SyntaxComponent } from './syntax/syntax.component';
import { TableComponent } from './table/table.component';
import { LocalVarsComponent } from './local-vars/local-vars.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'syntax' },
  { path: 'syntax', component: SyntaxComponent },
  { path: 'table', component: TableComponent },
  { path: 'vars', component: LocalVarsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NgForRoutingModule { }
