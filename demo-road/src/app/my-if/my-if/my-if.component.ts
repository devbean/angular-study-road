import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-if',
  templateUrl: './my-if.component.html',
  styleUrls: ['./my-if.component.css']
})
export class MyIfComponent implements OnInit {

  title = 'Custom Directives in Angular';
  show = true;

  constructor() { }

  ngOnInit(): void {
  }

}
