import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyIfComponent } from './my-if/my-if.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'myif' },
  { path: 'myif', component: MyIfComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyIfRoutingModule { }
