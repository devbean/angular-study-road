import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MyIfRoutingModule } from './my-if-routing.module';
import { MyIfDirective } from './my-if.directive';
import { MyIfComponent } from './my-if/my-if.component';


@NgModule({
  declarations: [MyIfDirective, MyIfComponent],
  imports: [
    CommonModule,
    FormsModule,
    MyIfRoutingModule
  ]
})
export class MyIfModule { }
