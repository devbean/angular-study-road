import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgSwitchRoutingModule } from './ngswitch-routing.module';
import { SyntaxComponent } from './syntax/syntax.component';



@NgModule({
  declarations: [
    SyntaxComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgSwitchRoutingModule
  ]
})
export class NgswitchModule { }
