import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SyntaxComponent } from './syntax/syntax.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'syntax' },
  { path: 'syntax', component: SyntaxComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NgSwitchRoutingModule { }
