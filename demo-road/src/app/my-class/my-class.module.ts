import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyClassRoutingModule } from './my-class-routing.module';
import { MyClassDirective } from './my-class.directive';
import { MyClassComponent } from './my-class/my-class.component';


@NgModule({
  declarations: [MyClassDirective, MyClassComponent],
  imports: [
    CommonModule,
    MyClassRoutingModule
  ]
})
export class MyClassModule { }
