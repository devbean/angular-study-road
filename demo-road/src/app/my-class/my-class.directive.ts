import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[myClass]'
})
export class MyClassDirective implements OnInit {

  @Input() myClass?: string;

  constructor(
    private readonly el: ElementRef
  ) { }

  ngOnInit(): void {
    if (this.myClass) {
      this.el.nativeElement.classList.add(this.myClass);
    }
  }

}
