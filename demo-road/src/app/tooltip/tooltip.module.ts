import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TooltipRoutingModule } from './tooltip-routing.module';
import { TooltipDirective } from './tooltip.directive';
import { TooltipComponent } from './tooltip/tooltip.component';


@NgModule({
  declarations: [TooltipDirective, TooltipComponent],
  imports: [
    CommonModule,
    TooltipRoutingModule
  ]
})
export class TooltipModule { }
